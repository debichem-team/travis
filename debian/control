Source: travis
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Daniel Leidert <dleidert@debian.org>
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: http://www.travis-analyzer.de
Vcs-Browser: https://salsa.debian.org/debichem-team/travis
Vcs-Git: https://salsa.debian.org/debichem-team/travis.git

Package: travis
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Suggests: cp2k,
          gnuplot,
          grace,
          graphviz,
          pymol
Description: trajectory analyzer and visualizer
 TRAVIS (Trajectory Analyzer and Visualizer) is a free tool for analyzing
 and visualizing trajectories from all kinds of Molecular Dynamics or
 Monte Carlo simulations. The aim of TRAVIS is to collect as many analyses
 as possible in one program, creating a powerful tool and making it
 unnecessary to use many different programs for evaluating simulations.
 This should greatly rationalize and simplify the workflow of analyzing
 trajectories. The following analysis functions are available:
 .
 Static (time independent) Functions:
  * Radial, Angular, Dihedreal or Combined Distribution Function
  * Point-Plane or Point-Line Distance Distribution
  * Plane Projection Distribution
  * Fixed Plane Density Profile
  * Density, Spatial or Dipole Distribution Function
 .
 Dynamic (time dependent) Functions:
  * Velocity Distribution Function
  * Mean Square Displacement / Diffusion Coefficients
  * Velocity Autocorrelation Functions
  * Vector Reorientation Dynamics
  * Van Hove Correlation Function
  * Aggregation Functions (DACF, DLDF, DDisp)
 .
 Spectroscopic Functions:
  * Calculate Power Spectrum
  * Calculate IR Spectrum
  * Calculate Raman Spectrum
 .
 TRAVIS can read trajectory files in XYZ, PDB, LAMMPS or DLPOLY format.
